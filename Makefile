.PHONY: all disk srpm rpm 

all:	
	echo "run: make dist|srpm|rpm"

clean: 
	rm -rf build/
	test -e cern-eos-s3.spec && unlink cern-eos-s3.spec || echo 

dist:   clean
	./make-dist.sh

srpm:	dist
	./make-srpm.sh

rpm:	srpm
	./make-rpm.sh

