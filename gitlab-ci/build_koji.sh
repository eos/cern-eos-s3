#!/usr/bin/env bash
exit
kinit stci@CERN.CH -k -t /stci.krb5/stci.keytab

build_koji() {   # args: source targetarch
    echo "Building ${1}/${2} in KOJI"
    koji build ${1} ${2}/SRPMS/*.src.rpm
}

build_koji eos7 cc7 
build_koji eos8 c8  

