#!/usr/bin/env bash
exit
kinit stci@CERN.CH -k -t /stci.krb5/stci.keytab

export_rpms() {   # args: source targetarch
    EXPORT_DIR=/eos/project/s/storage-ci/www/eos/citrine-depend/$2/x86_64/
    echo "Publishing from $1 in location: ${EXPORT_DIR}"
    mkdir -p ${EXPORT_DIR}
    cp $1/RPMS/*.rpm ${EXPORT_DIR}
    createrepo -q ${EXPORT_DIR}
}

export_rpms cc7 el-7
export_rpms c8  el-8

