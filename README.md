CERN package to run minio S3 gateways automatically with a per user key/secret on dedicated ports

How-To
=======
* deployment hosts need a wild card alias: *.host.domain for each gateway machine
* service can be run in parallel on several gateway machines
* service can be exposed using a toplevel DNS alias *.s3eos.cern.ch
* S3 gateway hosts must be added to each instance which is mounted by the S3 service as eosxd(unix) gateways
* the user S3 id and secrets are configured in /etc/eos/s3/minio.cfg

Users can access their buckets

## RPM cern-eos-s3-server

Contains the minio server packaged under /opt/eos/minio/bin/minio

## RPM cern-eos-s3-client

Contains the minio mc client packaged under /opt/eos/minio/bin/mc

## RPM cern-eos-s3-systemd

Contains the systemd unit s3eos and the required start/stop scripts under /sbin/

## RPM cern-eos-s3-config

Contains an example config file to configure the CERN S3 minio service.
