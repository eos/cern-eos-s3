#!/bin/bash
# ----------------------------------------------------------------------
# File: eos-start-minio-nas
# Author: Andreas-Joachim Peters - CERN
# ----------------------------------------------------------------------

# ************************************************************************
# * EOS - the CERN Disk Storage System                                   *
# * Copyright (C) 2021 CERN/Switzerland                                  *
# *                                                                      *
# * This program is free software: you can redistribute it and/or modify *
# * it under the terms of the GNU General Public License as published by *
# * the Free Software Foundation, either version 3 of the License, or    *
# * (at your option) any later version.                                  *
# *                                                                      *
# * This program is distributed in the hope that it will be useful,      *
# * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
# * GNU General Public License for more details.                         *
# *                                                                      *
# * You should have received a copy of the GNU General Public License    *
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
# **

# usage: eos-start-minio-nas <user> <mount> <s3-id> <s3-key>
# example: eos-start-minio-nas nobody eoshome-n.cern.ch:/eos/user/n/nobody foo bar

user=$1
mount=$2
id=$3
key=$4
port=${5-9000}
cport=${6-10000}
# prepare mounts
usermount=/s3eos/$user/
mkdir -p $usermount
pkill -9 -f "runuser -u $user"
pkill -9 -f "/opt/eos/minio/bin/minio gateway nas --address :$port --console-address :$cport $usermount"
pkill -f "eosxd $usermount"
umount -fl $usermount
mount -t fuse eosxd -ofsname=$mount $usermount
# prepare minio
LOGFILE=/var/log/minio/minio.nas.$user.log
mkdir -p /var/log/minio/

touch $LOGFILE
chown $user $LOGFILE

runuser -u $user eos-run-minio-nas "$id" "$key" $LOGFILE $usermount $port $cport $user &

echo "# user=%user mount=$usermount id=$id key=$key logfile=$LOGFILE"

pkill -9 "runuser -u $user"




